package com.goodfancier.searchEngine;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.SpringApplication;

@SpringBootApplication
public class SearchEngineApplication
{
    public static void main(String[] args)
    {
        SpringApplication.run(SearchEngineApplication.class, args);
    }
}